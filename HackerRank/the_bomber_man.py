#!/bin/python3

import math
import os
import random
import re
import sys
import copy

#
# Complete the 'bomberMan' function below.
#
# The function is expected to return a STRING_ARRAY.
# The function accepts following parameters:
#  1. INTEGER n
#  2. STRING_ARRAY grid
#
def str_arr_to_2d_char_list(grid):
    for i in range(len(grid)):
        grid[i] = list(grid[i])
    return grid
 
def twoD_char_list_to_str_arr(grid):
    for i in range(len(grid)):
        grid[i] = "".join(grid[i])
    return grid

def fill_grid(grid):
    for row in range(len(grid)):
            for col in range(len(grid[0])):
                grid[row][col] = 'O'
    return grid
    
def detonate(grid):
    temp = copy.deepcopy(grid)
    grid = fill_grid(grid)
    for row in range(len(grid)):
        for col in range(len(grid[0])):
            if temp[row][col] == 'O':
                grid[row][col] = '.'
                if row - 1 >= 0:
                    grid[row - 1][col] = '.'
                if row + 1 < len(grid):
                    grid[row + 1][col] = '.'
                if col - 1 >= 0:
                    grid[row][col - 1] = '.'
                if col + 1 < len(grid[0]):
                    grid[row][col + 1] = '.'
    return grid    
    
def bomberMan(n, grid):
    grid = str_arr_to_2d_char_list(grid)
    if n%2 == 0:
        return twoD_char_list_to_str_arr(fill_grid(grid))
    
    if n < 4:
        if n == 1:
            return twoD_char_list_to_str_arr(grid)
        return twoD_char_list_to_str_arr(detonate(grid))
        
    one_detonation = detonate(grid)
    two_detonations = detonate(one_detonation)
    
    if n%4 == 1:
        return twoD_char_list_to_str_arr(two_detonations)
    
    three_detonations = detonate(two_detonations)
    return twoD_char_list_to_str_arr(three_detonations)
    

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    r = int(first_multiple_input[0])

    c = int(first_multiple_input[1])

    n = int(first_multiple_input[2])

    grid = []

    for _ in range(r):
        grid_item = input()
        grid.append(grid_item)

    result = bomberMan(n, grid)

    fptr.write('\n'.join(result))
    fptr.write('\n')

    fptr.close()
