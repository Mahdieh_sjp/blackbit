def fun(s):
    # return True if s is a valid email, else return False
    try:
        username, rest = s.split('@')
        site, ext = rest.split('.')
    except ValueError:
        return False

    if username.replace('-', '').replace('_', '').isalnum() and site.isalnum() and ext.isalpha() and len(ext) < 4:
        return True
    
    return False
