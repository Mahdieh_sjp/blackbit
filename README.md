# BlackBit

### Answers to

##### [HackerRank](https://www.hackerrank.com)
1. [The Grid Search](https://www.hackerrank.com/challenges/the-grid-search/problem?isFullScreen=false)
2. [No Idea](https://www.hackerrank.com/challenges/no-idea/problem?isFullScreen=false)
3. [Bomber Man](https://www.hackerrank.com/challenges/bomber-man/problem?isFullScreen=false)
4. [Between Two Sets](https://www.hackerrank.com/challenges/between-twosets/problem?isFullScreen=false)
5. [Repeating String](https://www.hackerrank.com/challenges/repeated-string/problem?isFullScreen=false)
6. [Validate List of Email Address With Filter](https://www.hackerrank.com/challenges/validate-list-of-emailaddress-with-filter/problem?isFullScreen=false)
7. [Dealing With Complex Numbers](https://www.hackerrank.com/challenges/class-1-dealingwith-complex-numbers/problem?isFullScreen=false)

##### [Quera College](https://quera.org/college/)
8. [Chapter8684/lesson29941](https://quera.org/college/3078/chapter/8684/lesson/29941/?comments_page=1&comments_filter=ALL&submissions_page=1)
9. [Chapter9151/lesson103424](https://quera.org/college/3078/chapter/9151/lesson/103424/?comments_page=1&comments_filter=ALL&submissions_page=1)
10. [Chapter10367/lesson34774](https://quera.org/college/3078/chapter/10367/lesson/34774/?comments_page=1&comments_filter=ALL&submissions_page=1)


